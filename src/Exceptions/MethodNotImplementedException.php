<?php
namespace Tcrawf\Ci4Auth\Exceptions;

use Exception as VanillaException;

class MethodNotImplementedException extends VanillaException
{

}