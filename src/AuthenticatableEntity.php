<?php


namespace Tcrawf\Ci4Auth;

use Tcrawf\Ci4Auth\Contracts\AuthenticatableModel as Authenticatable;

trait AuthenticatableEntity
{



    public function getAuthIdentifier(Authenticatable $model)
    {
        return $this->{$model->getAuthIdentifierName()};
    }

    public function getAuthPassword()
    {
        return $this->password;
    }


    public function getRememberToken(Authenticatable $model)
    {
        if(!empty($model->getRememberTokenName())) {
            return (string) $this->{$model->getRememberTokenName()};
        }
    }

    public function setRememberToken(Authenticatable $model, $value)
    {
        if (! empty($model->getRememberTokenName())) {
            $this->{$model->getRememberTokenName()} = $value;
        }
    }

}
