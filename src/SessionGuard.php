<?php
namespace Tcrawf\Ci4Auth;

/**
 * This class provides authentication services.
 *
 * It is an adaptio of Illuminate\Auth\SessionGuard from Laravel to Codeigniter 4
 *
 */

use Tcrawf\Ci4Auth\Contracts\AuthenticatableEntity as AuthenticatableEntity;
use Tcrawf\Ci4Auth\Contracts\Guard;
use Tcrawf\Ci4Auth\Contracts\StatefulGuard;
use Tcrawf\Ci4Auth\Contracts\UserProvider;
use CodeIgniter\Events\Events;
use CodeIgniter\HTTP\RequestInterface as Request;
use CodeIgniter\Session\SessionInterface as Session;
use Config\Services;


class SessionGuard implements Guard, StatefulGuard
{
    use GuardHelpers;


    /**
     * Name of the guard, normally session
     *
     * @var string $name
     */
    protected $name;

    /**
     * The user we last attempted to retrieve
     *
     * @var  AuthenticatableEntity $lastAttempted;
     *
     */
    protected $lastAttempted;

    /**
     * Indicates if the user was authenticated via a recaller cookie.
     *
     * @var bool
     */
    protected $viaRemember = false;


    /**
     * The session used by the guard
     *
     * @var Session $session
     */
    protected $session;


    /**
     * The Codeigniter request instance
     *
     * @var Request $request
     */
    protected $request;


    /**
     * Indicates if the logout method has been called
     *
     * @var bool $loggedOut
     */
    protected $loggedOut = false;


    /**
     * Indicates if a token user retrieval has been attempted.
     *
     * @var bool
     */
    protected $recallAttempted = false;


    /**
     * Create new authentication guard.
     *
     * @param $name
     * @param UserProvider $provider
     * @param Session $session
     * @param Request|null $request
     * @return void
     */
    public function __construct($name,
                                UserProvider $provider,
                                Session $session,
                                Request $request = null )
    {

        $this->name = $name;
        $this->session = $session;
        $this->request = $request;
        $this->provider = $provider;//GuardHelpers
    }


    /**
     * Return currently authenticated user.
     *
     * @return AuthenticatableEntity |null
     */
    public function user()
    {
        if ($this->loggedOut) {
            return null;
        }

        // If we've already retrieved the user for the current request we can just
        // return it back immediately. We do not want to fetch the user data on
        // every call to this method because that would be tremendously slow.
        if (! is_null($this->user)) {
            return $this->user;
        }

        $id = $this->session->get($this->getName());

        // First we will try to load the user using the identifier in the session if
        // one exists. Otherwise we will check for a "remember me" cookie in this
        // request, and if one exists, attempt to retrieve the user using that.
        if (! is_null($id) && $this->user = $this->provider->retrieveById($id)) {
            $this->fireAuthenticatedEvent($this->user);
        }

        /*
        // If the user is null, but we decrypt a "recaller" cookie we can attempt to
        // pull the user data on that cookie which serves as a remember cookie on
        // the application. Once we have a user we can return it to the caller.
        if (is_null($this->user) && ! is_null($recaller = $this->recaller())) {
            $this->user = $this->userFromRecaller($recaller);

            if ($this->user) {
                $this->updateSession($this->user->getAuthIdentifier());

                $this->fireLoginEvent($this->user, true);
            }
        }*/

        return $this->user;

    }


    /**
     * Get the ID for the currently authenticated user
     *
     * @return int|null
     */
    public function id()
    {
        //Why is the id definitely an int? Could it not be a string?

        if($this->loggedOut)
        {
            return null;
        }


        return $this->user()
                    ? (int) $this->user()->getAuthIdentifier($this->provider->getModel())
                    : (int) $this->session->get($this->getName());

    }

    /**
     * Log a user into the application without sessions or cookies.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function once(array $credentials = [])
    {
        $this->fireAttemptEvent($credentials);

        if ($this->validate($credentials)) {
            $this->setUser($this->lastAttempted);

            return true;
        }

        return false;
    }


    /**
     * Log the given user ID into the application without sessions or cookies.
     *
     * @param  mixed  $id
     * @return AuthenticatableEntity|false
     */
    public function onceUsingId($id)
    {
        if (! is_null($user = $this->provider->retrieveById($id))) {
            $this->setUser($user);

            return $user;
        }

        return false;
    }


    /**
     * Validate a user's credentials.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function validate(array $credentials = [])
    {
        $this->lastAttempted = $user = $this->provider->retrieveByCredentials($credentials);

        return $this->hasValidCredentials($user, $credentials);
    }


    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function attempt(array $credentials = [])
    {
        $this->fireAttemptEvent($credentials);

        $this->lastAttempted = $user = $this->provider->retrieveByCredentials($credentials);

        // If an implementation of UserInterface was returned, we'll ask the provider
        // to validate the user against the given credentials, and if they are in
        // fact valid we'll log the users into the application and return true.
        if ($this->hasValidCredentials($user, $credentials)) {
            $this->login($user);

            return true;
        }

        // If the authentication attempt fails we will fire an event so that the user
        // may be notified of any suspicious attempts to access their account from
        // an unrecognized user. A developer may listen to this event as needed.
        $this->fireFailedEvent($user, $credentials);

        return false;
    }



    /**
     * Determine if the user matches the credentials.
     *
     * @param  mixed  $user
     * @param  array  $credentials
     * @return bool
     */
    protected function hasValidCredentials($user, $credentials)
    {
        return ! is_null($user) && $this->provider->validateCredentials($user, $credentials);
    }


    /**
     * Log the given user ID into the application.
     *
     * @param  mixed  $id
     * @param  bool   $remember
     * @return AuthenticatableEntity|false
     */
    public function loginUsingId($id, $remember = false)
    {
        if (! is_null($user = $this->provider->retrieveById($id))) {
            $this->login($user);

            return $user;
        }

        return false;
    }

    /**
     * Log a user into the application.
     *
     * @param AuthenticatableEntity $user
     * @param bool $remember
     * @return void
     */
    public function login(AuthenticatableEntity $user)
    {
        //Code smell: keep generating a model to just pass as dependency to the AuthenticatableEntity (refactor)
        $this->updateSession($user->getAuthIdentifier($this->provider->getModel()));

        /*
         * Support for "remember" functionality to be added subsquently
         */

        // If we have an event dispatcher instance set we will fire an event so that
        // any listeners will hook into the authentication events and run actions
        // based on the login and logout events fired from the guard instances.

        $this->fireLoginEvent($user);

        $this->setUser($user);
    }


    /**
     * Update the session with the given ID.
     *
     * @param  string  $id
     * @return void
     */
    protected function updateSession($id)
    {
        $this->session->set($this->getName(),$id);
        $this->session->regenerate('true');

    }


    /**
     * Log the user out of the application.
     *
     * @return void
     */
    public function logout()
    {
        $user = $this->user();

        // If we have an event dispatcher instance, we can fire off the logout event
        // so any further processing can be done. This allows the developer to be
        // listening for anytime a user signs out of this application manually.
        $this->clearUserDataFromStorage();

        Events::trigger('user_logout',$this->name,$user);

        // Once we have fired the logout event we will clear the users out of memory
        // so they are no longer available as the user is no longer considered as
        // being signed into this application and should not be available here.
        $this->user = null;

        $this->loggedOut = true;
    }

    /**
     * Remove the user data from the session and cookies.
     *
     * @return void
     */
    protected function clearUserDataFromStorage()
    {
        $this->session->remove($this->getName());

        /*
        if (! is_null($this->recaller())) {
            $this->getCookieJar()->queue($this->getCookieJar()
                ->forget($this->getRecallerName()));
        }*/
    }

    /**
     * Fire the authenticated event if the dispatcher is set.
     *
     * @param AuthenticatableEntity $user
     * @return void
     */
    protected function fireAuthenticatedEvent($user)
    {
        //Coupled through call to static function. Argggh.
        Events::trigger('user_authenticated',$user->getAuthIdentifier($this->provider->getModel())); //Add $this->getName() to string to prevent naming conflicts?

    }


    /**
     * Fire the attempt event with the arguments.
     *
     * @param  array  $credentials
     * @return void
     */
    protected function fireAttemptEvent(array $credentials)
    {
        Events::trigger('user_authentication_attempt',$this->name,$credentials);
    }

    /**
     * Fire the failed event
     *
     * @param AuthenticatableEntity $user | null
     * @param array $credentials
     */
    protected function fireFailedEvent(?AuthenticatableEntity $user, array $credentials)
    {
        Events::trigger('user_failed_authentication',
                                   $this->name,
                                   $user,
                                   $credentials);

    }


    protected function fireLoginEvent(AuthenticatableEntity $user)
    {
        Events::trigger('user_login',$this->name,$user);

    }


    /**
     * Get the last user we attempted to authenticate.
     *
     * @return AuthenticatableEntity
     */
    public function getLastAttempted()
    {
        return $this->lastAttempted;
    }


    /**
     * Get the unique identifier for the auth session value
     *
     * @return string
     */
    private function getName()
    {
        return 'login_'.$this->name.'_'.sha1(static::class);
    }

    /**
     * Get the session store used by the guard.
     *
     * @return Session
     */
    public function getSession()
    {
        return $this->session;
    }


    /**
     * Return the currently cached user.
     *
     * @return AuthenticatableEntity|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the current user
     *
     * @param AuthenticatableEntity $user
     * @return $this
     */


    public function setUser(AuthenticatableEntity $user)
    {
        $this->user = $user;

        $this->loggedOut = false;

        $this->fireAuthenticatedEvent($user);

        return $this;

    }

    /**
     * Get the current request instance.
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->request ?: Services::request();
    }


    /**
     * Set the current request instance
     *
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

}

//TODO:: Refactor to add the auth library to a composer package
//TODO:: Refactor to implement Guard and Stateful guard interfaces
//TODO:: Add support for basic authentication
//TODO:: Add functionality to logout other devices (invalidate all sessions for the current user)
//TODO:: Decouple Events functionality