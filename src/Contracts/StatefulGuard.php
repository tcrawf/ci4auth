<?php

namespace TCrawf\Ci4Auth\Contracts;

use TCrawf\Ci4Auth\Contracts\AuthenticatableEntity as Authenticatable;

interface StatefulGuard extends Guard
{
    /**
     * Attempt to authenticate a user using the given credentials.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function attempt(array $credentials = []);

    /**
     * Log a user into the application without sessions or cookies.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function once(array $credentials = []);

    /**
     * Log a user into the application.
     *
     * @param  Authenticatable  $user
     * @return void
     */
    public function login(Authenticatable $user);

    /**
     * Log the given user ID into the application.
     *
     * @param  mixed  $id
     * @return Authenticatable
     */
    public function loginUsingId($id);

    /**
     * Log the given user ID into the application without sessions or cookies.
     *
     * @param  mixed  $id
     * @return bool
     */
    public function onceUsingId($id);


    /**
     * Log the user out of the application.
     *
     * @return void
     */
    public function logout();
}
