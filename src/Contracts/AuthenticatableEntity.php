<?php
namespace Tcrawf\Ci4Auth\Contracts;

use TCrawf\Ci4Auth\Contracts\AuthenticatableModel as Authenticatable;

interface AuthenticatableEntity
{
    /**
     * Get the unique identifier for the user.
     *
     * @param AuthenticatableModel $model
     * @return mixed
     */
    public function getAuthIdentifier(Authenticatable $model);


    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword();

    /**
     * Get the token value for the "remember me" session.
     *
     * @param AuthenticatableModel $model
     * @return string
     */
    public function getRememberToken(Authenticatable $model);

    /**
     * Set the token value for the "remember me" session.
     *
     * @param AuthenticatableModel $model
     * @param string $value
     * @return void
     */
    public function setRememberToken(Authenticatable $model, $value);


}
