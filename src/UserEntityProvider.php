<?php

namespace TCrawf\Ci4Auth;

use Tcrawf\Ci4Auth\Contracts\AuthenticatableEntity as Authenticatable;
use Tcrawf\Ci4Auth\Contracts\AuthenticatableModel as AuthenticatableModel;
use Tcrawf\Ci4Auth\Contracts\UserProvider;


class UserEntityProvider implements UserProvider
{

    /**
     * The name of the Authenticatable Model
     *
     * @var string $model
     */
    protected $model;

    public function __construct(string $model)
    {
        $this->model = $model;
    }

    /**
     * Retrieve a user by their unique identifier
     *
     * @param $identifier
     * @return Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        $model = $this->createModel();

        /** @var Authenticatable $entity */
        $entity = $model->where($model->getAuthIdentifierName(),$identifier)->first();

        return $entity;
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param mixed $identifier
     * @param string $token
     * @return Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        /** @var AuthenticatableModel $model */
        $model = $this->createModel();

        /** @var Authenticatable $entity */
        $entity = $model->where($model->getAuthIdentifierName(),$identifier)->first();

        if(is_null($entity))
        {
            return null;
        }

        //Cannot
        $rememberToken = $entity->getRememberToken($model);

        return $rememberToken && hash_equals($rememberToken, $token) ? $entity : null;
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param Authenticatable $user
     * @param string $token
     * @return void
     * @throws \ReflectionException
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        $model = $this->createModel();

        $user->setRememberToken($model,$token);

        $model->save($user);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param array $credentials
     * @return Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (empty($credentials) ||
            (count($credentials) === 1 &&
                array_key_exists('password', $credentials))) { //credentials = identifier + password
            return null;
        }

        // First we will add each credential element to the query as a where clause.
        // Then we can execute the query and, if we found a user, return it in a
        // an Entity that will be utilized by the Guard instances.
        $model = $this->createModel();

        foreach ($credentials as $key => $value) {


            if (mb_strpos($key,'password')!== false) {
                continue;
            }

            //Codeigniter entities have toArray method and so we can ,
            // but don't have this as an interface. Will remove the check on arrayable

            if (is_array($value) /*|| $value instanceof Arrayable*/) {
                $model->whereIn($key, $value);
            } else {
                $model->where($key, $value);
            }
        }

        /** @var Authenticatable $entity */
        $entity = $model->first();
        return $entity;
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param Authenticatable $user
     * @param array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $plain = $credentials['password'];
        return password_verify($plain,$user->getAuthPassword());
    }

    /**
     * Create a new instance of the model.
     *
     * @return UserModel
     */
    public function createModel()
    {

        $class = '\\'.ltrim($this->model, '\\');

        //Code smell: Passing string instead of object or factory.
        //We are not checking that the model is of the correct type

        $model = new $class;

        //Now added check of type, but we are binding here. User can create a new provider if they wish.
        if(!($model instanceof UserModel))
        {
            throw New \InvalidArgumentException(
                "The model string must resolve to a type of ".__NAMESPACE__.'UserModel');
        }

        return $model;
    }

    /**
     * Get an Authenticatable model
     *
     * @return AuthenticatableModel
     */
    public function getModel()
    {
        //Code smell - breaking encapsulation

        return $this->createModel();
    }

    /**
     * Gets the name of the UserModel
     *
     * @return string
     */
    public function getModelName()
    {
        return $this->model;
    }



    /**
     * Sets the name of the UserModel
     *
     * (should have returnType of Authenticatable Entity)
     * (should extend Codeigniter Model)
     *
     * @param string $model
     */
    public function setModelName(string $model)
    {
        $this->model = $model;
    }
}