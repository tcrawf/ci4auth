<?php


namespace Tcrawf\Ci4Auth;

use Tcrawf\Ci4Auth\Contracts\AuthenticatableModel as Authenticatble;
use CodeIgniter\Model;

class UserModel extends Model implements Authenticatble
{
    protected $table = 'user';

    protected $returnType = 'App\Entities\User';

    use AuthenticatableModel;

}