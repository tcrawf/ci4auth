#Authentication module for Codeigniter4 based on Laravel
 
Laravel makes authentication of users very easy. This package provides functionality from Laravel for authentication 
of users within Codeigniter 4 (CI4). Once the library has stabilized I will add it to packagist.org 
 
_Usage instructions_
 
* Add the repository to your composer.json file.
  
````  
    "repositories": [
         {
             "type": "vcs",
             "url" : "git@bitbucket.org:tcrawf/ci4auth.git"
         }
     ],
````     
 
* Require the package 
 
```` 
    composer require tcrawf/ci4auth:@dev
    
````    
 
* Add a service locator function to App\Config\Services.php

````

    use Tcrawf\Ci4Auth\Contracts\UserProvider;
    use Tcrawf\Ci4Auth\SessionGuard;
    use Tcrawf\Ci4Auth\UserEntityProvider;
 
    use CodeIgniter\Config\Services as CoreServices;
 
    use CodeIgniter\HTTP\RequestInterface;
    use CodeIgniter\Session\SessionInterface;
 
    require_once SYSTEMPATH . 'Config/Services.php';
 
    class Services extends CoreServices
    {
     
     public static function auth(string $name = null,
                                 UserProvider $provider = null,
                                 SessionInterface $session = null,
                                 RequestInterface $request = null,
                                 bool $getShared = true)
     {
 
         if ($getShared)
         {
             return static::getSharedInstance('auth', $name,$provider,$session,$request);
         }
 
         $name = $name ?? 'session';
         $session = $session ?? static::session();
         $request = $request ?? static::request();
         $provider = $provider ?? new UserEntityProvider('App\Models\UserModel'); //We are currently getting a shared instance, but should consider
 
         return new SessionGuard($name,$provider,$session,$request);
      }
 
    }
````    
 
* Create a user model that extends Tcrawf\Ci4Auth\UserModel

```` 
 
    <?php
 
    namespace App\Models;
 
 
    use Tcrawf\Ci4Auth\UserModel as AuthenticatableUserModel;
 
 
    class UserModel extends AuthenticatableUserModel
    {
 
 
 
    }
 
````    
 
 * Create an entity  that implements the AuthenticatbleEntity contract. This can be done by using the 
 AuthenticatableEntity trait provided with the library.
 
````
    
    namespace App\Entities;
 
    use Tcrawf\Ci4Auth\AuthenticatableEntity;
    use Tcrawf\Ci4Auth\Contracts\AuthenticatableEntity as Authenticatable;
 
    use CodeIgniter\Entity;
 
    class User extends Entity implements Authenticatable
    {
        use AuthenticatableEntity;
 
        protected $id;
        protected $name;
        protected $email;
        protected $password;
        protected $email_verified_at;
        protected $remember_token;
 
    } 
````
     
 
 * (Optional) Create another EntityProvider if required (e.g. if you wish to use a vanilla model rather than CI4 model)
 and modify the service locator
 
 
 __Usage__
 
 The library can then be used instantiated within your base controller, instantiated directly within your controller 
 or added to a filter. 
 
For using filters [see the CI4 user guide](https://codeigniter4.github.io/userguide/incoming/filters.html?highlight=filter)

  
Using the helper
```` 
    $auth = service('auth');
````  
OR, the static method
 
````    
    $auth = Services::auth();
 
     if ($auth->guest()) 
    {
        return redirect('login');
    }
````    

 To attempt to login:

````
    $attemptSuccessful = $auth->attempt(['id'=> (int) $id, 'password'=>'password']);

```` 
  
  If anyone wants to contribute, please create a pull request. 
  
  The 'remember me' functionality from Laravel will be
  added as time permits. I will also write a wrapper to make event triggering optional and allow another event 
  system to be used if required.